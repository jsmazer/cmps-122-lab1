#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <crypt.h>

char combinations[63] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
			            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
			            '0','1','2','3','4','5','6','7','8','9','\0'};


void bruteForce(char guess[], char salt[], char *cryptPasswd, char *passwd){
    for(int i=0;i<62;i++)
      for(int j=0;j<62;j++)
        for(int k=0;k<62;k++)
          for(int l=0;l<62;l++) {
            guess[0] = combinations[i];
            guess[1] = combinations[j];
            guess[2] = combinations[k];
            guess[3] = combinations[l];

            char *encrypt_guess = crypt(guess, salt);
            int ret = strcmp(encrypt_guess, cryptPasswd);
            if(ret == 0){
                strcpy(passwd, guess);
                return;
            }

          }
}

void getCrypt(char line[], char cryptPasswd[]){
    int size = strlen(line);

    int start = -1;
    int end = -1;

    for(int i=0;i<size;i++) {
        char c = line[i];
        if(c == ':' && start == -1) {
            start = i;
        } else if(c == ':' && start != -1 && end == -1) {
            end = i;
        }
    }

    int length = end - start;
    strncpy(cryptPasswd, line + start + 1, length-1);
}

/*
 * Find the plain-text password PASSWD of length PWLEN for the user USERNAME
 * given the encrypted password CRYPTPASSWD.
 */
void crackSingle(char *username, char *cryptPasswd, int pwlen, char *passwd) {

	/* Test different passwords by doing brute force then encrypt it
	using the crypt() function using the salt. The salt is the first
	two letters of the username. Afterwards, compare the encryption
	of your password to the cryptPasswd provided above. If they both match
	then the password you guessed is correct. Finally, copy that password
	into the passwd parameter above. */

	char salt[3];
	strncpy(salt, username, 2);

	char guess[5];
    guess[4] = '\0';

    bruteForce(guess, salt, cryptPasswd, passwd);
}

/*
 * Find the plain-text passwords PASSWDS of length PWLEN for the users found
 * in the old-style /etc/passwd format file at pathe FNAME.
 */
void crackMultiple(char *fname, int pwlen, char **passwds) {
    FILE *fd;
    fd = fopen(fname, "r");

    char line[128];

    int index  = 0;
    if(fd != NULL) {
        while(fgets(line, sizeof(line), fd) != NULL) {

            char salt[3];
            salt[2] = '\0';
            strncpy(salt, line, 2);

            char passwd[5];
            passwd[4] = '\0';

            char guess[5];
            guess[4] = '\0';

            char cryptPasswd[14];
            cryptPasswd[13] = '\0';
            getCrypt(line, cryptPasswd);

            bruteForce(guess, salt, cryptPasswd, passwd);

            strcpy(passwds[index], passwd);
            index++;

            //printf("Password: %s\n", passwd);
            //printf("Line: %s", line);
            //printf("CryptPasswd: %s\n", cryptPasswd);
            //printf("Salt: %s\n", salt);
        }
    } else {
        perror("The following error occured");
    }

    fclose(fd);
}

/*
 * Find the plain-text passwords PASSWDS of length PWLEN for the users found
 * in the old-style /etc/passwd format file at pathe FNAME.
 */
void crackSpeedy(char *fname, int pwlen, char **passwds) {
    crackMultiple(fname, pwlen, passwds);
 }

/*
 * Find the plain-text password PASSWD of length PWLEN for the user USERNAME 
 * given the encrypted password CRYPTPASSWD without using more than MAXCPU
 * percent of any processor.
 */
void crackStealthy(char *username, char *cryptPasswd, int pwlen, char *passwd, int maxCpu) {
    crackSingle(username, cryptPasswd, pwlen, passwd);
}
